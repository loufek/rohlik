package com.rohlik.example.service;

import com.rohlik.example.exception.InsufficientStockSizeException;
import com.rohlik.example.exception.InvalidOrderStateException;
import com.rohlik.example.model.dto.OrderDto;
import com.rohlik.example.model.entity.Order;
import com.rohlik.example.model.entity.OrderItem;
import com.rohlik.example.model.entity.Product;
import com.rohlik.example.repository.OrderRepository;
import com.rohlik.example.repository.ProductRepository;
import org.junit.function.ThrowingRunnable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class OrderServiceTest {

    @MockBean
    private ProductRepository productRepositoryMock;

    @MockBean
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @BeforeEach
    public void setup() {
        Product p = new Product();
        p.setId(1L);
        p.setName("test");
        p.setPrice(10);
        p.setStockSize(10);

        when(productRepositoryMock.findAllById(Collections.singleton(1L))).thenReturn(Collections.singletonList(p));
        when(orderRepository.save(any())).then(i -> i.getArgument(0, Order.class) );

        Order o = new Order();
        o.setId(1L);
        o.setState(Order.State.CREATED);
        o.setBlockedUntil(Instant.now().plus(30, ChronoUnit.MINUTES));
        OrderItem orderItem = new OrderItem();
        orderItem.setAmount(10);
        orderItem.setProduct(p);
        o.setItems(Collections.singletonList(orderItem));

        Order o2 = new Order();
        o2.setId(2L);
        o2.setState(Order.State.PAID);


        when(orderRepository.findByIdAndUser(eq(1L), any())).thenReturn(Optional.of(o));
        when(orderRepository.findByIdAndUser(eq(2L), any())).thenReturn(Optional.of(o2));
        when(orderRepository.findByIdAndUser(eq(3L), any())).thenReturn(Optional.empty());
    }

    @Test
    void createOrder() {
        OrderDto request = OrderDto.builder()
                .address("address")
                .items(Collections.singletonMap(1L, OrderDto.OrderItemDto.builder().amount(5).build()))
                .build();
        OrderDto result = orderService.createOrder(request, "user", false);
        assertEquals(50, result.getPrice(), 0);

        request.getItems().get(1L).setAmount(50);
        assertThrows(InsufficientStockSizeException.class, (ThrowingRunnable) () -> orderService.createOrder(request, "user", true));
        result = orderService.createOrder(request, "user", false);
        assertEquals(Order.State.INSUFFICIENT_NUMBER.name(), result.getState().name());
    }

    @Test
    void setOrderState() {
        Order order = orderService.setOrderState(1L, "username", Order.State.CANCELED);
        assertEquals(Order.State.CANCELED.name(), order.getState().name());
        assertEquals(20, order.getItems().get(0).getProduct().getStockSize());
        assertThrows(InvalidOrderStateException.class, () -> orderService.setOrderState(2L, "username", Order.State.CANCELED));
        assertThrows(EntityNotFoundException.class, () -> orderService.setOrderState(3L, "username", Order.State.CANCELED));
    }
}