--liquibase formatted sql
--changeset data-seed:1 splitStatements:true

insert into user ( name,password ,role, username) values
('admin', '$2a$10$xGLwh2DGAAOr6BaYU/Z0Pe9mfbpziWcL8ninkp.pVifavrcwPtRkK', 'ROLE_ADMIN', 'admin');

insert into user ( name,password ,role, username) values
('user', '$2a$10$B6nebVCOYOiLhe2WMri/.uDmbvhekKYzzqjz3oYldkDh5gAmSUNYq', 'ROLE_USER', 'user');

insert into product ( name,price ,stock_size, deleted) values ('Milk', 10, 20, false);
insert into product ( name,price ,stock_size, deleted) values ('Sugar', 20, 20, false);
insert into product ( name,price ,stock_size, deleted) values ('Water', 5, 20, false);


