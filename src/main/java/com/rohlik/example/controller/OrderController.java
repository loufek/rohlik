package com.rohlik.example.controller;

import com.rohlik.example.model.dto.OrderDto;
import com.rohlik.example.model.entity.Order;
import com.rohlik.example.model.entity.User;
import com.rohlik.example.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@Validated
public class OrderController {

    private final OrderService orderService;

    @Operation(summary = "Create new order")
    @Secured({User.ROLE_USER})
    @PostMapping("/order")
    public ResponseEntity<OrderDto> createOrder(
            @Valid @RequestBody
            @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = OrderDto.class),
                            examples = {
                                    @ExampleObject(name = "Example order", value = "{\n" +
                                            "  \"address\": \"Testovaci 123/45, Liberec\",\n" +
                                            "  \"items\": {\n" +
                                            "    \"1\": {\n" +
                                            "      \"amount\": 10\n" +
                                            "    }\n" +
                                            "  }\n" +
                                            "}\n"),
                                    @ExampleObject(name = "Example order - insufficient stock size", value = "{\n" +
                                            "  \"address\": \"Testovaci 123/45, Liberec\",\n" +
                                            "  \"items\": {\n" +
                                            "    \"1\": {\n" +
                                            "      \"amount\": 10\n" +
                                            "    }\n" +
                                            "  }\n" +
                                            "}\n"),
                            })
            ) OrderDto newOrderDto, Principal principal) {
        OrderDto order = orderService.createOrder(newOrderDto, principal.getName(), false);

        return new ResponseEntity<>(
                order,
                Order.State.INSUFFICIENT_NUMBER.equals(order.getState()) ? HttpStatus.BAD_REQUEST : HttpStatus.OK
        );
    }

    @Operation(summary = "Create new order - error message")
    @Secured({User.ROLE_USER})
    @PostMapping("/order2")
    public OrderDto createOrder2(@Valid @RequestBody OrderDto newOrderDto, Principal principal) {
        return orderService.createOrder(newOrderDto, principal.getName(), true);
    }

    @Operation(summary = "My orders")
    @Secured({User.ROLE_USER})
    @GetMapping("/me/order")
    public List<OrderDto> getMyOrdersOrder(Principal principal) {
        return orderService.getMyOrders(principal.getName());
    }

    @Operation(summary = "All orders")
    @Secured({User.ROLE_ADMIN})
    @GetMapping("/admin/order")
    public List<OrderDto> getAllOrders() {
        return orderService.getAllOrders();
    }

    @Operation(summary = "Mark my order as paid")
    @Secured({User.ROLE_USER})
    @PutMapping("/order/{id}/paid")
    public void setOrderAsPaid(@PathVariable Long id, Principal principal) {
        orderService.setOrderState(id, principal.getName(), Order.State.PAID);
    }

    @Operation(summary = "Cancel my order")
    @Secured({User.ROLE_USER})
    @PutMapping("/order/{id}/cancel")
    public void setOrderAsCanceled(@PathVariable Long id, Principal principal) {
        orderService.setOrderState(id, principal.getName(), Order.State.CANCELED);
    }
}
