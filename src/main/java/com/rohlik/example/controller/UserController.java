package com.rohlik.example.controller;

import com.rohlik.example.model.dto.RegisterUserDto;
import com.rohlik.example.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class UserController {

    private final UserService userService;

    @Operation(summary = "Register user")
    @PostMapping("/user/register")
    public RegisterUserDto createProduct(@RequestBody RegisterUserDto registerUserDto) {
        return userService.registerUser(registerUserDto);
    }
}
