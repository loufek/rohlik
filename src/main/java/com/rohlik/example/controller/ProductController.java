package com.rohlik.example.controller;

import com.rohlik.example.model.dto.OrderDto;
import com.rohlik.example.model.dto.ProductDto;
import com.rohlik.example.model.entity.User;
import com.rohlik.example.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@Validated
public class ProductController {

    private final ProductService productService;

    @Operation(summary = "Create new product")
    @Secured({User.ROLE_ADMIN})
    @PostMapping("/product")
    public ProductDto createProduct(
            @Valid @RequestBody
            @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = OrderDto.class),
                            examples = {
                                    @ExampleObject(name = "Example product", value = "{\n" +
                                            "  \"name\": \"Some product\",\n" +
                                            "  \"price\": 10,\n" +
                                            "  \"stockSize\": 200\n" +
                                            "}")
                            })
            ) ProductDto productDto) {
        return productService.createProduct(productDto);
    }

    @Operation(summary = "Update product")
    @Secured({User.ROLE_ADMIN})
    @PutMapping("/product/{id}")
    public ProductDto updateProduct( @PathVariable Long id, @RequestBody ProductDto productDto) {
        return productService.updateProduct(id, productDto);
    }

    @Operation(summary = "Soft-delete product")
    @Secured({User.ROLE_ADMIN})
    @DeleteMapping("/product/{id}")
    public void deleteProduct( @PathVariable Long id) {
        productService.removeProduct(id);
    }

    @Operation(summary = "Get particular product")
    @GetMapping("/product/{id}")
    public ProductDto getProduct(@PathVariable Long id) {
        return productService.getProduct(id);
    }

    @Operation(summary = "Get all products")
    @GetMapping("/product")
    public List<ProductDto> getAllProducts() {
        return productService.getAllProducts();
    }
}
