package com.rohlik.example.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Value("${app.token.accessTokenUri}")
    private String accessTokenUri;

    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI().info(
                new Info().title("Rohlik API").description("Rohlik")
        ).components(new Components()
                .addSecuritySchemes("oAuthScheme", new SecurityScheme()
                        .in(SecurityScheme.In.QUERY)
                        .type(SecurityScheme.Type.OAUTH2)
                        .flows(new OAuthFlows().password(new OAuthFlow().tokenUrl(accessTokenUri)))
                )
        ).addSecurityItem(new SecurityRequirement().addList("oAuthScheme"));
    }
}
