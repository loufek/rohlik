package com.rohlik.example.repository;

import com.rohlik.example.model.entity.Order;
import com.rohlik.example.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByUser(User user);
    Optional<Order> findByIdAndUser(Long id, User user);
    List<Order> findAllByBlockedUntilBefore(Instant date);
}
