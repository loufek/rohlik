package com.rohlik.example.exception;

import com.rohlik.example.model.dto.OrderDto;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Map;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InsufficientStockSizeException extends RuntimeException {

    @Getter
    private Map<Long, OrderDto.OrderItemDto> items;

    public InsufficientStockSizeException(String s, Map<Long, OrderDto.OrderItemDto> items) {
        super(s);
        this.items = items;
    }
}
