package com.rohlik.example.service;

import com.rohlik.example.model.converter.ProductConverter;
import com.rohlik.example.model.dto.ProductDto;
import com.rohlik.example.model.entity.Product;
import com.rohlik.example.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductConverter productConverter;

    public ProductDto createProduct(ProductDto productDto) {
        if (productDto.getId() != null) {
            throw new ValidationException("invalid input");
        }
        Product p = productRepository.save(productConverter.convert(productDto));
        productDto.setId(p.getId());

        return productDto;
    }

    public ProductDto updateProduct(Long id, ProductDto productDto) {
        if (productDto.getId() == null || productDto.getId() != id) {
            throw new ValidationException("invalid input");
        }
        productRepository.save(productConverter.convert(productDto));

        return productDto;
    }

    public void removeProduct(Long id) {
        productRepository.deleteById(id);
    }

    public ProductDto getProduct(Long id) {
        return  productRepository.findById(id)
                .map(productConverter::convert)
                .orElseThrow(() -> new EntityNotFoundException("Product not found"));
    }

    public List<ProductDto> getAllProducts() {
        return  productRepository.findAll().stream()
                .map(productConverter::convert)
                .collect(Collectors.toList());
    }
}
