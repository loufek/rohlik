package com.rohlik.example.service;

import com.rohlik.example.model.dto.RegisterUserDto;
import com.rohlik.example.model.entity.User;
import com.rohlik.example.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public RegisterUserDto registerUser(RegisterUserDto registerUserDto) {
        User user = new User();
        user.setRole(User.ROLE_USER);
        user.setPassword(passwordEncoder.encode(registerUserDto.getPassword()));
        user.setUsername(registerUserDto.getUsername());
        user.setName(registerUserDto.getName());

        userRepository.save(user);

        return registerUserDto;
    }
}
