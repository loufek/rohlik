package com.rohlik.example.service;

import com.rohlik.example.exception.InsufficientStockSizeException;
import com.rohlik.example.exception.InvalidOrderStateException;
import com.rohlik.example.model.converter.OrderConverter;
import com.rohlik.example.model.dto.OrderDto;
import com.rohlik.example.model.entity.Order;
import com.rohlik.example.model.entity.Product;
import com.rohlik.example.repository.OrderRepository;
import com.rohlik.example.repository.ProductRepository;
import com.rohlik.example.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final OrderConverter orderConverter;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    @Transactional
    public OrderDto createOrder(OrderDto orderDto, String username, boolean throwOnError) {

        List<Product> products = productRepository.findAllById(orderDto.getItems().keySet());
        orderDto.getItems().forEach((key, value) -> {
            Product product = products.stream().filter(p -> p.getId().equals(key)).findFirst()
                    .orElseThrow(() -> new EntityNotFoundException("Product " + key + " not found"));
            value.setProduct(product);
        });

        for (OrderDto.OrderItemDto orderItem: orderDto.getItems().values()) {
            if (orderItem.getAmount() > orderItem.getProduct().getStockSize()) {
                orderItem.setMaxSize(orderItem.getProduct().getStockSize());
                orderDto.setState(Order.State.INSUFFICIENT_NUMBER);
            }
        }

        if (Order.State.INSUFFICIENT_NUMBER.equals(orderDto.getState())) {
            if (throwOnError){
                throw new InsufficientStockSizeException("Insufficient stock size for products", orderDto.getItems());
            }
              return orderDto;
        }
        Order order = orderConverter.convert(orderDto);
        order.setUser(userRepository.findOneByUsername(username));
        order.getItems().forEach(orderItem ->
                orderItem.getProduct().setStockSize(orderItem.getProduct().getStockSize() - orderItem.getAmount())
        );

        return orderConverter.convert(orderRepository.save(order));
    }

    public List<OrderDto> getMyOrders(String username) {
        return orderRepository.findAllByUser(userRepository.findOneByUsername(username))
                .stream().map(orderConverter::convert)
                .collect(Collectors.toList());
    }

    public List<OrderDto> getAllOrders() {
        return orderRepository.findAll()
                .stream().map(orderConverter::convert)
                .collect(Collectors.toList());
    }

    @Transactional
    public Order setOrderState(Long id, String username, Order.State state){
        Order order = orderRepository.findByIdAndUser(id, userRepository.findOneByUsername(username))
                .orElseThrow(() -> new EntityNotFoundException("Order with id " + id + " not found"));
        if (!order.getState().equals(Order.State.CREATED)) {
            throw new InvalidOrderStateException("Order must must be in created state");
        }
        order.setState(state);
        order.setBlockedUntil(null);
        if (state.equals(Order.State.CANCELED)) {
            order.getItems().forEach(orderItem ->
                orderItem.getProduct().setStockSize(orderItem.getProduct().getStockSize() + orderItem.getAmount())
            );
        }
        return orderRepository.save(order);
    }

    /**
     * Check invalid orders once per minute
     */
    @Transactional
    @Scheduled(fixedRate = 60000)
    public void invalidateOrders(){
        for (Order order: orderRepository.findAllByBlockedUntilBefore(Instant.now())) {
            order.getItems().forEach(orderItem ->
                    orderItem.getProduct().setStockSize(orderItem.getProduct().getStockSize() + orderItem.getAmount())
            );
            order.setState(Order.State.INVALID);
            orderRepository.save(order);
        }
    }
}
