package com.rohlik.example.model.converter;

import com.rohlik.example.model.dto.ProductDto;
import com.rohlik.example.model.entity.Product;
import com.rohlik.example.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
public class ProductConverter {

    private final ProductRepository productRepository;

    public ProductDto convert(Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .stockSize(product.getStockSize())
                .price(product.getPrice())
                .build();
    }

    public Product convert(ProductDto productDto) {

        Product product = null;
        if (productDto.getId() != null) {
            product = productRepository.findById(productDto.getId())
                    .orElseThrow(() -> new EntityNotFoundException("Product not found"));
        } else {
            product = new Product();
        }
        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());
        product.setStockSize(productDto.getStockSize());

        return product;
    }
}
