package com.rohlik.example.model.converter;

import com.rohlik.example.model.dto.OrderDto;
import com.rohlik.example.model.entity.Order;
import com.rohlik.example.model.entity.OrderItem;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderConverter {

    private static final int BLOCK_MINUTES = 30;

    public OrderDto convert(Order order) {
        return OrderDto.builder()
                .id(order.getId())
                .address(order.getAddress())
                .blockedUntil(order.getBlockedUntil())
                .price(order.getPrice())
                .state(order.getState())
                .items(order.getItems().stream().map(i ->
                        OrderDto.OrderItemDto.builder()
                                .amount(i.getAmount())
                                .product(i.getProduct())
                                .build()
                ).collect(Collectors.toMap(o -> o.getProduct().getId(), o -> o)))
                .build();
    }

    public Order convert(OrderDto orderDto) {
        Order order = new Order();
        order.setState(Order.State.CREATED);
        order.setAddress(orderDto.getAddress());
        order.setBlockedUntil(Instant.now().plus(BLOCK_MINUTES, ChronoUnit.MINUTES));

        double price = orderDto.getItems().values().stream()
                .mapToDouble(i -> i.getAmount() * i.getProduct().getPrice())
                .sum();
        order.setPrice(price);

        List<OrderItem> items = orderDto.getItems().values().stream()
                .map(orderItem -> OrderItem.builder()
                        .amount(orderItem.getAmount())
                        .order(order)
                        .product(orderItem.getProduct())
                        .build()
                ).collect(Collectors.toList());
        order.setItems(items);

        return order;
    }
}
