package com.rohlik.example.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@DynamicUpdate
@Table(name = "\"order\"")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double price;

    private String address;

    private State state;

    private Instant blockedUntil;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy="order", cascade = CascadeType.ALL)
    private List<OrderItem> items;

    public enum State {
        CREATED, PAID, CANCELED, INVALID, INSUFFICIENT_NUMBER

    }
}
