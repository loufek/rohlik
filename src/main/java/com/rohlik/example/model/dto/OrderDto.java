package com.rohlik.example.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rohlik.example.model.entity.Order;
import com.rohlik.example.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @NotEmpty @NotNull
    private String address;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Order.State state;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Double price;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Instant blockedUntil;

    @NotEmpty @NotNull
    private Map<Long, OrderItemDto> items;

    @JsonIgnore
    private boolean valid;

    @Data
    @Valid
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class OrderItemDto {
        @NotNull @Min( value = 1)
        int amount;
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        public String getName() {
            return product == null ? null : product.getName();
        }

        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        Integer maxSize;
        @JsonIgnore
        Product product;
    }
}
