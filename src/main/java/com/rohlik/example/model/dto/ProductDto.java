package com.rohlik.example.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Valid
public class ProductDto {

    private Long id;

    @NotNull @NotEmpty
    private String name;

    @Min(value = 0)
    @NotNull
    private double price;

    @Min(value = 0)
    @NotNull
    private int stockSize;
}
